#############################################################################
# Some helpers to make HTTP requests and examine responses

import logging


# Make an HTTP request.
def http_request(ctx, host=None, method=None, url=None):
    runcmd_run = globals()["runcmd_run"]
    runcmd_exit_code_is = globals()["runcmd_exit_code_is"]
    logging.debug(f"Make HTTP request: {method} {url}")
    runcmd_run(ctx, ["curl", "-ksv", "-X", method, f"-HHost: {host}", url])
    runcmd_exit_code_is(ctx, 0)


# Check status code of latest HTTP request.
def http_status_code_is(ctx, code=None):
    assert_eq = globals()["assert_eq"]
    runcmd_get_stderr = globals()["runcmd_get_stderr"]
    stderr = runcmd_get_stderr(ctx)
    logging.debug(f"Verifying status code of previous HTTP request is {code}")
    logging.debug(f"  stderr={stderr}")
    pattern = f"\n< HTTP/2 {code} "
    assert_eq(pattern in stderr, True)


# Check a HTTP response header for latest request has a given value.
def http_header_is(ctx, header=None, value=None):
    assert_eq = globals()["assert_eq"]
    runcmd_get_stderr = globals()["runcmd_get_stderr"]
    stderr = runcmd_get_stderr(ctx)
    logging.debug(f"Verifying response has header {header}: {value}")
    pattern = f"\n< {header}: {value}"
    assert_eq(pattern in stderr, True)


# Check a HTTP body response  for latest request has a given value.
def http_body_is(ctx, body=None):
    assert_eq = globals()["assert_eq"]
    runcmd_get_stdout = globals()["runcmd_get_stdout"]
    stdout = runcmd_get_stdout(ctx)
    logging.debug(f"Verifying response body is {body!r}")
    logging.debug(f"  actual body={stdout!r}")
    body = body.encode("UTF8").decode("unicode-escape")
    assert_eq(body, stdout)
