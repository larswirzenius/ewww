# ewww -- a web server for static content

ewww is a web server for static content, and static content only. It
is in its very early stages.

Written in Rust for safety, security, and performance.

Source code is at <https://gitlab.com/larswirzenius/ewww>, and issues
and patches should be filed there, if possible.

## Legalese

ewww is licensed under the Affero General Public Licence, version 3 or
later.

Copyright (C) 2020, 2021  Lars Wirzenius

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
